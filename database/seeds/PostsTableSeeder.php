<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'My first blog post',
                'body' => 'Hello there welcome to my new blog',
                'user_id' => 1,
                'created_at' => Carbon::now(),
            ],
            [
                'title' => 'My second blog post',
                'body' => 'Hello there welcome to my new blog',
                'user_id' => 2,
                'created_at' => Carbon::now(),
            ],
            [
                'title' => 'My third blog post',
                'body' => 'Motivational blog post',
                'user_id' => 1,
                'created_at' => Carbon::now(),
            ]
        ]);
    }
}
