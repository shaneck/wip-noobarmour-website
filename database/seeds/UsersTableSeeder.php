<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Shane Kenyon',
                'email' => 'shane@aol.com',
                'password' => bcrypt('password'),
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Matthew Griffin',
                'email' => 'matt@aol.com',
                'password' => bcrypt('password123'),
                'created_at' => Carbon::now(),
            ]
        ]);
    }
}
